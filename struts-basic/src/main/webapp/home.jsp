<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <html:form action="/home.do">
      <fieldset>
        <bean:message key="global.lbl.name"/>: <html:text property="name" />
        <br />
        <bean:message key="global.lbl.alias"/>: <html:text property="username" />
        <br />
        <bean:message key="global.lbl.age"/>: <html:text property="age" />
        <br />
        <bean:message key="global.lbl.roles"/>:
        <br />
        <logic:iterate id="role" name="frmProfile" property="roles">
          <bean:write name="role" />
        </logic:iterate>
      </fieldset>
      <html:submit>
        <bean:message key="global.lbl.update" />
      </html:submit>
    </html:form>
  </body>
</html>