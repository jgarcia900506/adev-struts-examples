<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <html:errors />
    <html:form action="/login.do" method="POST">
      <fieldset>
        <bean:message key="login.lbl.username" />:
        <html:text property="username" />
        <br />
        <bean:message key="login.lbl.password" />:
        <html:password property="password" />
        <br />
      </fieldset>
      <html:submit>
        <bean:message key="login.btn.command" />
      </html:submit>
    </html:form>
  </body>
</html>