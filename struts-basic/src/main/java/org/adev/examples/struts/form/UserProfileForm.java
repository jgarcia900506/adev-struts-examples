package org.adev.examples.struts.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class UserProfileForm extends ActionForm {

	private String			name;
	private String			username;
	private Integer 		age;
	private List<String>	roles;
	private boolean			enabled;
	
	private static final long serialVersionUID = -707635973308796444L;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public void enable() {
		enabled = true;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public String toString() {
		return String.format("{name: %s, username: %s, age: %d, roles: %s}", name, username, age, roles.toString());
	}
	
}
