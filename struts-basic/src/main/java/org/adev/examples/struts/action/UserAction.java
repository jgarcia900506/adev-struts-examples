package org.adev.examples.struts.action;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.adev.examples.struts.form.UserProfileForm;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class UserAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		UserProfileForm raw = (UserProfileForm) form;
		
		if(!raw.isEnabled()) {
			raw.setName("Juan Garcia");
			raw.setUsername("jgarcia");
			raw.setAge(29);
			raw.setRoles(Arrays.asList(new String[] {
				"USER",
				"ADMIN"
			}));
			raw.enable();
		}
		
		return mapping.findForward("success");
	}
	
}
