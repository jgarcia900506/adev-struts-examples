package org.adev.examples.struts.form;

import org.apache.struts.validator.ValidatorForm;

public class AuthenticationForm extends ValidatorForm {
	
	private String username;
	private String password;
	
	private static final long serialVersionUID = -1407834122034843028L;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
