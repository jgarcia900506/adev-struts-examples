package org.adev.examples.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.adev.examples.struts.form.AuthenticationForm;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class AuthenticationAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		AuthenticationForm	authentication	= (AuthenticationForm) form;
		ActionForward		forward			= mapping.findForward("failure");
		
		if(authentication != null) {
			forward = mapping.findForward("success");
		}
		
		return forward;
	}
	
}
