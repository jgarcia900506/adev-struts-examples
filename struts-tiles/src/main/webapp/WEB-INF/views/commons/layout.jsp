<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>
      <tiles:getAsString name="title"/>
    </title>
  </head>
  <body>
    <tiles:insert attribute="header" />
    <tiles:insert attribute="body" />
    <tiles:insert attribute="footer" />
  </body>
</html>